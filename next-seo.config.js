const description =
  "Launch your own fully automated store with Snipcart, Printful, and Next.js";
const title = "Dassiet Store";
const url = "https://www.dassiet.shop";

const seo = {
  title,
  titleTemplate: "Dassiet store",
  description,
  openGraph: {
    description,
    title,
    type: "website",
    url,
  },
};

export { seo as defaultSeo, url as defaultUrl };
