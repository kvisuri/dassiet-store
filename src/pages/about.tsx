function TermsPage() {
  return (
    <div className="max-w-xl mx-auto prose prose-blue">
      <h2>Welcome</h2>
      <p>
        This is the official Dassiet merchandise store, meant for employees and their families and friends.
      </p>
      <h3>
        How long does ordering take?
      </h3>
      <p>
        Each item is made-to-order and shipped directly to your home. Your products should arrive within 1-2 weeks. You'll see a more detailed estimate at the checkout page.
      </p>
      <h3>
        What's the shipping cost?
      </h3>
      <p>
        Shipping has a flat rate cost of about 7-15€ to Europe currently.
      </p>
    </div>
  );
}

export default TermsPage;
